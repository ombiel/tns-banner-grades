var request = require("-aek/request");
var _ = require("-aek/utils");

var AekStorage = require("-aek/storage");
var storage = new AekStorage("Tulane-Banner-Grades");

class GradesUtils {

  fetchData(cb) {
    // request.get(_.publicPath("/data/test.json")).end((err, response) => {
    request.action('fetch-info').end((err, response) => {
      // console.log(response);
      var body = JSON.parse(response.body.json);
      if(!_.isEmpty(body)) {
        storage.set('semesters', body);
        cb(null, body);
      } else {
        cb(true, null);
      }
    });
  }

  cachedData(cb) {
    if(storage.get('semesters') !== null) {
      var semesters = storage.get('semesters');
      // console.log(semesters);
      cb(null, semesters);
      return semesters;
    } else {
      cb(true, null);
      return null;
    }
  }

  //Data comes back differently and isn't sorted into semesters so this function returns
  //all the courses which are in the same semester
  fetchGrades(code,cb) {
    let semester = storage.get('semesters').filter(course => course.term.code === code);
    if (semester.length > 0) {
      cb(null, semester);
    } else {
      cb(true, null);
    }
  }

}

// export a single instance so all modules see the share the same thing
module.exports = new GradesUtils();

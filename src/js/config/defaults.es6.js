// Default text for all static content set here
module.exports = {
  IndexPage:{
    header: 'Terms',
    blurb: 'Please choose a term to view grades:',
    error: 'No semesters found',
    fatal: 'No data found'
  },
  TermPage:{
    labels:{
      gradeFinal: 'Final Grade',
      gradeInAcadHistory: 'Grade'
    },
    results:{
      error: 'Grade not available'
    },
    error: 'No grades found',
    fatal: 'No data found'
  }
};

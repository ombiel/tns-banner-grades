var React = window.React = require("react");
var Page = require("-components/page");
var {VBox,Panel} = require("-components/layout");
var {BannerHeader,Header} = require("-components/header");
var {BasicSegment} = require("-components/segment");
var {Listview} = require("-components/listview");
var {InfoMessage} = require("-components/message");
var _ = require("-aek/utils");

var Utils = require("../scripts/utils");

var TermPage = React.createClass({

  getInitialState:function() {
    return {
      loading:true,
      error:false
    };
  },

  componentDidMount:function() {
    this.getGradesByTerm();
    this.forceUpdate();
  },

  getGradesByTerm: function(){
    var code = this.props.ctx.params.id;
    if(code) {
      Utils.fetchGrades(code,(err,response)=>{
        if(!err && response) {
          this.setState({response:response,loading:false});
        }
        else {
          this.setState({error:true,loading:false});
        }
      });
    }
    else {
      this.setState({error:true,loading:false});
    }
  },

  render:function() {

    var header;
    let content = [];

    let loading = this.state.loading;
    let response = this.state.response;
    let config = this.props.config;

    if(this.state.error || !response && !loading) {

      content.push(<InfoMessage>{config.TermPage.fatal}</InfoMessage>);

    }
    else if(!loading && !this.state.error) {

      header = response[0].term ? response[0].term.description ? <BannerHeader theme="prime" key="header" flex={0} level="2">{response[0].term.description}</BannerHeader> : "" : "";

      let course = response;

      if(!course) {
        content.push(<InfoMessage>{config.TermPage.error}</InfoMessage>);
      }
      else {

        let dataItems = course;
        let total = dataItems ? dataItems.length : false;
        let results,length;

        if(!_.isEmpty(dataItems)) {
          if(!total) {
            // Object
            results = [];
            results.push(dataItems);
            length = results.length;
          }
          else {
            // Array
            results = dataItems;
            length = results.length;
          }
        }
        else {
          // When data is an empty object or returns undefined
          length = 0;
        }

        // Show results
        if(length > 0)
        {
          results.forEach((data)=>{
            let items = [];
            // Courses
            if(data.courseNumber && data.courseTitle) {
              if(data.gradeInAcadHistory) {
                items.push({
                  label:config.TermPage.labels.gradeFinal,
                  text:data.gradeInAcadHistory ? data.gradeInAcadHistory : "-",
                  key: data.courseNumber + "course-final-"
                });
              }
            }
            // Build listview from Courses array
            if(items.length > 0) {
              content.push(<Header block level="4" key={"header" + data.courseNumber}><b>{data.courseTitle}</b></Header>);
              content.push(<Listview key={data.courseNumber} items={items} uniformLabels="true" className={'tns-grade'} />);
            }
            else {
              content.push(<InfoMessage key={"error" + data.courseNumber} className={"no-grade"}><b>{data.courseTitle}</b><br /><i>{config.TermPage.results.error}</i></InfoMessage>);
            }
          });
        }

      }

    }

    return (
      <Page>
        <VBox>
          <Panel>
            {header}
            <BasicSegment loading={loading}>
              {content}
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );

  }

});

module.exports = TermPage;

var React = window.React = require("react");
var Page = require("-components/page");
var {VBox,Panel} = require("-components/layout");
var {BasicSegment} = require("-components/segment");
var {Listview} = require("-components/listview");
var {InfoMessage} = require("-components/message");
var {BannerHeader,Header} = require("-components/header");
var _ = require("-aek/utils");

var Utils = require("../scripts/utils");

// Android scroll bug fix
require("-aek/patches/fix-android-pull-to-refresh");

var IndexPage = React.createClass({

  getInitialState:function() {
    return {
      loading:true,
      error:false
    };
  },

  componentDidMount:function() {
    this.getSemesterList();
    this.forceUpdate();
  },

  getSemesterList: function(){
    var isCached = Utils.cachedData((err,response)=>{
      if(err) {
        Utils.fetchData((err,response)=>{
          if(!err && response) {
            this.setState({response:response,loading:false});
          }
          else {
            this.setState({error:true,loading:false});
          }
        });
      }
      else if(response) {
        this.setState({response:response,loading:false});
      }
    });

    // If data was cached data, lets try and update it in the background
    if(isCached) {
      this.setState({loading:true});
      Utils.fetchData((err,response)=>{
        if(!err && response) {
          this.setState({response:response,loading:false});
        }
        else {
          this.setState({error:true,loading:false});
        }
      });
    }
  },

  getTermById: function(code,ev){
    if(ev) {
      ev.preventDefault();
    }
    this.props.ctx.router.goto("/term/" + code);
  },

  render:function() {

    let content = [];

    let loading = this.state.loading;
    let response = this.state.response;
    let config = this.props.config;

    if(this.state.error || !response && !loading) {

      content.push(<InfoMessage>{config.IndexPage.fatal}</InfoMessage>);

    }
    else if(!loading && !this.state.error) {

      let semester = !_.isEmpty(response) ? response : false;

      if(!semester) {
        content.push(<InfoMessage>{config.IndexPage.error}</InfoMessage>);
      }
      else {

        let dataItems = response;
        let total = dataItems ? dataItems.length : false;
        let results,length;

        if(!_.isEmpty(dataItems)) {
          if(!total) {
            // Object
            results = [];
            results.push(dataItems);
            length = results.length;
          }
          else {
            // Array
            results = dataItems;
            length = results.length;
          }
        }
        else {
          // When data is an empty object or returns undefined
          length = 0;
        }

        // Show results
        if(length > 0)
        {
          var items = [];
          results.forEach((data,i)=>{
            // Semester
            var code = data.term.code;
            //Only add terms that don't exist yet.
            if (!items.find(o => o.text === data.term.description)){
              items.push({
                key: "term" + code,
                onClick: this.getTermById.bind(this,code),
                text: data.term.description,
                icon: "calendar"
              });
            }
          });

          //Sort so that the most recent tems is on top.
          items.sort((a,b) => (a.key > b.key) ? -1 : ((b.key > a.key) ? 1 : 0));

          // Build listview from Semester items array
          content.push(<p key="blurb" className={'blurb'}><b>{config.IndexPage.blurb}</b></p>);
          content.push(<Listview items={items} uniformLabels="true" key={"listview"} />);
        }

      }

    }

    return (
      <Page>
        <VBox>
          <Panel>
            <BannerHeader theme="prime" key="header" flex={0} level="2">{config.IndexPage.header}</BannerHeader>
            <BasicSegment loading={loading}>
              {content}
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );

  }

});

module.exports = IndexPage;

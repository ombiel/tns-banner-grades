// React 15
var React = window.React = require("react");
var reactRender = require("-aek/react/utils/react-render");

// Components
var {AekReactRouter,RouterView} = require("-components/router");
var router = new AekReactRouter();
var Container = require("-components/container");
var {VBox} = require("-components/layout");

// Pages
var IndexPage = require("./pages/index");
var TermPage = require("./pages/term");

// Default text for all static content set here
var Config = require("./config/defaults");

var Screen = React.createClass({

  render:function() {

    return (
      <Container>
        <VBox>
          <RouterView router={router}>
            <IndexPage path="/" config={Config} />
            <TermPage path="/term/:id/" config={Config} />
          </RouterView>
        </VBox>
      </Container>
    );
  }
});


reactRender(<Screen/>);
